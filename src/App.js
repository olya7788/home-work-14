import { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import "./App.css";
import ProductList from "./Components/ProductList/ProductList";
import FetchProducts from "./Components/FetchProducts/FetchProducts";
import { Layout } from "./pages/Layout";
import { Home } from "./pages/Home";
import { About } from "./pages/About";
import { Dashboard } from "./pages/Dashboard";
import { NoMatch } from "./pages/NoMatch";
import Product from "./Components/Product/Product";

function App() {
  
  const [tovari, setTovari] = useState([]);

 
  const [searchValue, setSearchValue] = useState("");

 
  const [foundedProducts, setFoundedProducts] = useState([]);

 
  useEffect(() => {
    setFoundedProducts(tovari);
  }, [tovari]);

 
  const titles = [];

  if (tovari.length > 0) {
    for (let i = 0; i < tovari.length; i++) {
      titles.push({ title: tovari[i].title.toLowerCase(), id: tovari[i].id });
    }
  }

  const foundSearchIds = [];
  if (searchValue.length >= 3) {
    titles.forEach((item) => {
      if (item.title.includes(searchValue.toLowerCase())) {
        foundSearchIds.push(item.id);
      }
    });
  }


  let tempFoundProducts = [];
  tovari.forEach((item) => {
    if (foundSearchIds.includes(item.id)) {
      tempFoundProducts.push(item);
    }
  });

 
  useEffect(() => {
    if (searchValue.length >= 3) {
      setFoundedProducts([...tempFoundProducts]);
    } else {
      setFoundedProducts(tovari);
    }
  }, [searchValue]);

  
  const categories = ["Все товары"];
 
  if (tovari.length > 0) {
    for (let i = 0; i < tovari.length; i++) {
      if (!categories.includes(tovari[i].category)) {
        categories.push(tovari[i].category);
      }
    }
  }
 
  const [selectValue, setSelectValue] = useState("Все товары");

  let tempFoundProductsFromSelect = [];
  tovari.forEach((item) => {
    if (item.category === selectValue) {
      tempFoundProductsFromSelect.push(item);
    }
  });

  useEffect(() => {
    if (selectValue !== "Все товары") {
      setFoundedProducts([...tempFoundProductsFromSelect]);
    } else {
      setFoundedProducts(tovari);
    }
  }, [selectValue]);

  return (
    <div className="App">
      <ProductList
        tovari={foundedProducts}
        searchValue={searchValue}
        setSearchValue={setSearchValue}
        categories={categories}
        selectValue={selectValue}
        setSelectValue={setSelectValue}
      />

      <FetchProducts setTovari={setTovari} />

      <div>
        <h1>Basic Example</h1>

        <p>
          This example demonstrates some of the core features of React Router
          including nested <code>&lt;Route&gt;</code>s,{" "}
          <code>&lt;Outlet&gt;</code>s, <code>&lt;Link&gt;</code>s, and using a
          "*" route (aka "splat route") to render a "not found" page when
          someone visits an unrecognized URL.
        </p>

        {/* Routes nest inside one another. Nested route paths build upon
            parent route paths, and nested route elements render inside
            parent route elements. See the note about <Outlet> below. */}
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route path="product-list" element={<ProductList />} />
            <Route path="product" element={<Product />} />
            <Route
              path="product/:id"
              element={<Product allProducts={tovari} />}
            />

            {/* <Route index element={<Home />} />
            <Route path="about" element={<About />} />
            <Route path="dashboard" element={<Dashboard />} /> */}

            {/* Using path="*"" means "match anything", so this route
                acts like a catch-all for URLs that we don't have explicit
                routes for. */}
            <Route path="*" element={<NoMatch />} />
          </Route>
        </Routes>
      </div>
    </div>
  );
}

export default App;

