import { useEffect, useState } from 'react';
import "./ProductList.scss";
import Product from "../Product/Product";

function ProductList({ tovari, searchValue, setSearchValue, categories, selectValue, setSelectValue }) {
  const [options, setOptions] = useState([]);

  useEffect(() => {
    if (categories !== undefined) {
      setOptions(categories.map((item, index) => <option key={index}>{item}</option>))
    }
  }, [categories]);

  return (
    <div className="product-list">
      <h1 className="product-list__h1">Список товаров</h1>

      <input
        type="text"
        className="product-list__search"
        placeholder="Поиск по товарам"
        value={searchValue}
        onChange={(event) => setSearchValue(event.target.value)}
      ></input>

      <select className="product-list__select" value={selectValue} onChange={(event) => setSelectValue(event.target.value)}>
        {options}
      </select>

      <div className="product-list__list-wrapper">
        {tovari && tovari.map((item) =>
          <Product product={item} key={item.id} />
        )}
      </div>

    </div>
  );
}

export default ProductList;
