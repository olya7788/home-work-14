import { useEffect } from "react";
import "./FetchProducts.scss";
import Button from '@mui/material/Button';

function FetchProducts({ setTovari }) {
  const getGoodsFromBackend = () => {
    let url = "https://fakestoreapi.com/products/";

    fetch(url)
      .then((response) => response.json())
      .then((response) => setTovari(response))
      .catch((e) => {
        console.log(e);
        alert("url не правильный!");
      });
  };

  useEffect(() => {
    getGoodsFromBackend();
  }, []);

  return (<div className="fetch-products__wrapper">
    <Button
      className="fetch-products__button"
      variant="contained"
      onClick={getGoodsFromBackend}>
      Загрузить товары
    </Button>
  </div>
  );
}

export default FetchProducts;
